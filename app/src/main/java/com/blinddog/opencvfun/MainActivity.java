package com.blinddog.opencvfun;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity implements CvCameraViewListener2 {

    private CameraBridgeViewBase cameraView;
    private Mat mainMatrix;
    private Mat matrixB;
    private MatOfRect results;
    private Bitmap bmp;
    private Bitmap texture;
    private Canvas canvas;
    private Paint p;
    private CascadeClassifier cc;
    private int scale;
    private float textureRatio;


    private BaseLoaderCallback cameraLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status){
                case LoaderCallbackInterface.SUCCESS:
                    try{
                        InputStream is = getResources().openRawResource(R.raw.haarcascade_frontalface_default);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        File cascadeXml = new File(cascadeDir, "cascade.xml");
                        FileOutputStream fos = new FileOutputStream(cascadeXml);
                        byte[] buff = new byte[4096];
                        int bytesRead;
                        while((bytesRead = is.read(buff)) != -1){
                            fos.write(buff, 0, bytesRead);
                        }
                        is.close();
                        fos.close();
                        cc = new CascadeClassifier(cascadeXml.getAbsolutePath());
                    }catch(Exception e){
                        Log.d("Loader", e.getMessage());
                    }
                    cameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraView = (JavaCameraView) findViewById(R.id.camera_view);
        cameraView.setVisibility(SurfaceView.VISIBLE);
        cameraView.setCvCameraViewListener(this);
        p = new Paint();
        p.setColor(Color.MAGENTA);
        try{
            texture = BitmapFactory.decodeResource(getResources(), R.drawable.texture);
            textureRatio = (float)texture.getHeight() / texture.getWidth();
        }catch(Exception e){

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!OpenCVLoader.initDebug()){
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, cameraLoaderCallback);
        }
        else{
            cameraLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(cameraView != null){
            cameraView.disableView();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(cameraView != null){
            cameraView.disableView();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mainMatrix = new Mat(height, width, CvType.CV_8UC4);
        scale = (int) (height * 0.2);
    }

    @Override
    public void onCameraViewStopped() {
        mainMatrix.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mainMatrix = inputFrame.rgba();
        results = new MatOfRect();


        Core.transpose(mainMatrix, mainMatrix);
        Core.flip(mainMatrix, mainMatrix, -1);
        matrixB = mainMatrix.clone();

        bmp = Bitmap.createBitmap(matrixB.cols(), matrixB.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(matrixB, bmp);
        canvas = new Canvas(bmp);
        Imgproc.cvtColor(mainMatrix, mainMatrix, Imgproc.COLOR_BGR2GRAY);

        if (cc != null) {
            cc.detectMultiScale(mainMatrix, results, 1.5, 5, 0, new Size(scale, scale), new Size(500, 500));
        }


        if (!results.empty()) {
            Rect r = results.toArray()[0];
            canvas.drawBitmap(Bitmap.createScaledBitmap(texture, r.width, (r.height / 4), false), (float) r.tl().x, (float) r.tl().y, p);
        }
        Utils.bitmapToMat(bmp, matrixB);
        return  matrixB;
    }
}
